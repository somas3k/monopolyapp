package pl.lab.view;

import pl.lab.MainApp;
import pl.lab.model.fieldpanes.FieldPane;
import pl.lab.model.fieldpanes.FieldPaneFactory;
import java.util.ArrayList;

class BoardSetter {
    private ArrayList<FieldPane> arrayOfFields;
    private BoardController boardController;
    private GameFX gameFX;

    BoardSetter(GameFX gameFX, BoardController boardController){
        this.gameFX=gameFX;
        this.boardController=boardController;
        setBoard();
        setFields();
    }
    private void setBoard(){
        boardController.getBoard().setStyle(" -fx-background-image: url('" +MainApp.class.getResource("/board.png").toExternalForm() +"');  "
                + "-fx-background-position: center center; "+"-fx-background-size: cover;"
                + "-fx-background-repeat: stretch;");

    }

    private void setFields(){
        arrayOfFields = FieldPaneFactory.createArrayFieldPane();
        for(int i =8;i >=0;--i){
            boardController.getLeft().getChildren().add(arrayOfFields.get(i));
        }
        for(int i =9;i<=19;++i){
            boardController.getTop().getChildren().add(arrayOfFields.get(i));
        }
        for(int i =20;i<=28;++i){
            boardController.getRight().getChildren().add(arrayOfFields.get(i));
        }
        for(int i =39;i>=29;--i){
            boardController.getBottom().getChildren().add(arrayOfFields.get(i));
        }
        gameFX.setFieldPanes(arrayOfFields);

    }

}
