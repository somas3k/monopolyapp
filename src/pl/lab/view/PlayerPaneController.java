package pl.lab.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import pl.lab.model.Player;


public class PlayerPaneController {
    @FXML
    private Label playerName;
    @FXML
    private Label accountStatement;
    @FXML
    private Circle pawn;

    private Player owner;

    //private Circle pawn;

    public Player getOwner() {
        return owner;
    }

    void setOwner(Player owner) {
        this.owner = owner;
    }

    void init(){
        playerName.setText(owner.getName());
        accountStatement.setText(""+owner.getBalance());
        pawn.setFill(Paint.valueOf(owner.getColour()));
        owner.setBalanceLabel(accountStatement);
    }

}
