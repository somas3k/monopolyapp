package pl.lab.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;


public class BoardController {
    @FXML
    private Pane preview;
    @FXML
    private Label owner;
    @FXML
    private HBox bottom;
    @FXML
    private HBox top;
    @FXML
    private VBox left;
    @FXML
    private VBox right;
    @FXML
    private Pane board;
    @FXML
    private Button closePreview;
    @FXML
    private ImageView dice_1;
    @FXML
    private ImageView dice_2;
    @FXML
    private Pane showChanceOrSocialCashCard;
    @FXML
    private Label title;
    @FXML
    private Label description;
    @FXML
    private Button execCommand;
    @FXML
    private Button takeChanceCard;
    @FXML
    private Button takeSocialCashCard;
    @FXML
    private Button buyButton;
    @FXML
    private Button sellButton;

    HBox getBottom() {
        return bottom;
    }

    HBox getTop() {
        return top;
    }

    VBox getLeft() {
        return left;
    }

    VBox getRight() {
        return right;
    }

    Pane getBoard() {
        return board;
    }

    Pane getPreview() {
        return preview;
    }

    Label getOwner() {
        return owner;
    }

    Button getClosePreview() {
        return closePreview;
    }

    ImageView getDice_1() {
        return dice_1;
    }

    ImageView getDice_2() {
        return dice_2;
    }

    Pane getShowChanceOrSocialCashCard() {
        return showChanceOrSocialCashCard;
    }

    Label getTitle() {
        return title;
    }

    public Label getDescription() {
        return description;
    }

    Button getExecCommand() {
        return execCommand;
    }

    Button getTakeChanceCard() {
        return takeChanceCard;
    }

    Button getTakeSocialCashCard() {
        return takeSocialCashCard;
    }

    Button getBuyButton() {
        return buyButton;
    }

    Button getSellButton() {
        return sellButton;
    }
}
