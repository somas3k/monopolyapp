package pl.lab.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pl.lab.MainApp;
import pl.lab.model.Dice;
import pl.lab.model.Game;
import pl.lab.model.Player;
import pl.lab.model.fieldpanes.FieldPane;
import pl.lab.model.fields.PossibleToBuyField;

import java.io.IOException;
import java.util.ArrayList;



public class GameFX {
    private MainController mainController;
    private BoardController boardController;
    private ArrayList<FieldPane> fieldPanes;
    private Game game;
    private Pane preview;
    private Button closePreview;
    private Label owner;
    private VBox rightPlayerPanes;
    private VBox leftPlayerPanes;
    private int numberOfPlayers;
    private ImageView dice_1;
    private ImageView dice_2;
    private Label actualPlayer;
    private Button diceButton;
    private Button nextPlayer;
    private Button startButton;
    private BorderPane gamePane;
    private Button moveButton;
    private Button chanceCardButton;
    private Button socialCashCardButton;
    private Pane showChanceOrSocialCashCard;
    private Label title;
    private Label description;
    private Button execCommandButton;
    private Button buyButton;
    private Button sellButton;
    private PossibleToBuyField actualPossibleToBuyField;
    private boolean isDoublet;


    public GameFX(Game game, MainController mainController){
        this.game=game;
        this.mainController=mainController;
        initialize();
    }
    private void initialize(){
        initializeExitButtons();
        new PlayerChooser(mainController,this,game);
        initializeBoard();
        initFieldActions();
        initializeComponents();

    }

    private void initializeExitButtons(){
        mainController.getCloseStage().setOnAction(event->handleCloseStage());
        mainController.getCloseStage11().setOnAction(event->handleCloseStage());
    }

    private void initializeBoard(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/BoardLayout.fxml"));
            BorderPane board = loader.load();
            boardController=loader.getController();
            new BoardSetter(this,boardController);
            mainController.getGamePane().setCenter(board);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initFieldActions(){

        for(FieldPane t : fieldPanes){
            t.addEventFilter(MouseEvent.MOUSE_PRESSED, event-> handleClickOnField(event.getSource()));
        }
    }

    private void initializeComponents(){
        gamePane=mainController.getGamePane();
        startButton=mainController.getStartButton();
        startButton.setOnAction(event->startGame());
        preview=boardController.getPreview();
        closePreview=boardController.getClosePreview();
        closePreview.setOnAction(event->handleClosePreview());
        owner=boardController.getOwner();
        dice_1=boardController.getDice_1();
        dice_2=boardController.getDice_2();
        actualPlayer=mainController.getActualPlayer();
        diceButton=mainController.getDiceButton();
        diceButton.setOnAction(event -> handleDiceButton());
        moveButton=mainController.getMoveButton();
        moveButton.setOnAction(event -> game.move(false));
        chanceCardButton=boardController.getTakeChanceCard();
        chanceCardButton.setOnAction(event -> showChanceCard());
        socialCashCardButton=boardController.getTakeSocialCashCard();
        socialCashCardButton.setOnAction(event -> showSocialCashCard());
        showChanceOrSocialCashCard=boardController.getShowChanceOrSocialCashCard();
        title=boardController.getTitle();
        description=boardController.getDescription();
        execCommandButton=boardController.getExecCommand();
        execCommandButton.setOnAction(event -> handleExecCommandButton());
        nextPlayer=mainController.getNextPlayer();
        nextPlayer.setOnAction(event -> game.nextPlayer());
        buyButton=boardController.getBuyButton();
        buyButton.setOnAction(event -> handleBuyButton());
        sellButton=boardController.getSellButton();
        sellButton.setOnAction(event -> handleSellButton());

    }



    private void handleExecCommandButton(){
        if(title.getText().equals("SZANSA")){
            game.execChanceCard();
            chanceCardButton.setDisable(true);
        }
        else{
            game.execSocialCashCard();
            socialCashCardButton.setDisable(true);
        }
        showChanceOrSocialCashCard.setVisible(false);

    }

    private void handleBuyButton(){

        if(game.buyField(actualPossibleToBuyField)) {
            owner.setText(game.getActualPlayer().getName());
            buyButton.setDisable(true);
            sellButton.setDisable(false);
        }
        else showAlert();
    }

    private void handleSellButton(){
        game.sellField(actualPossibleToBuyField);
        sellButton.setDisable(true);
    }

    private void handleDiceButton(){
        int[] t = game.rollTheDice();
        showDices(game.getDices().get(t[0]),game.getDices().get(t[1]));
        game.checkRollTheDice();

    }

    private void showDices(Dice dice1,Dice dice2){
        dice_1.setImage(dice1.getImage());
        dice_2.setImage(dice2.getImage());
    }

    private void startGame(){
        setActualPlayer(game.getPlayerName(0));
        startButton.setDisable(true);
        startButton.setVisible(false);
        gamePane.setDisable(false);
        gamePane.setOpacity(1);

    }

    public void showNextPlayerButton(){
        nextPlayer.setDisable(false);
    }

    public void canMove(){
        diceButton.setDisable(true);
        moveButton.setDisable(false);
    }

    public void canRollTheDice(){
        diceButton.setDisable(false);
    }

    public void hideNextPlayerButton(){
        nextPlayer.setDisable(true);
    }

    void setPawnsOnStartPane(){
        for(int i=0; i<numberOfPlayers;++i){
            ((VBox) fieldPanes.get(39).getChildren().get(0)).getChildren().get(i).setVisible(true);
        }
    }

    public void setPawnOnPane(int idOfActuallyPane,int newIdOfActuallyPane){
        if(idOfActuallyPane==40){
            ((VBox) fieldPanes.get(9).getChildren().get(1)).getChildren().get(game.getActualPlayerID()).setVisible(false);
        }
        else ((VBox) fieldPanes.get(idOfActuallyPane).getChildren().get(0)).getChildren().get(game.getActualPlayerID()).setVisible(false);
        if(newIdOfActuallyPane==40){
            ((VBox) fieldPanes.get(9).getChildren().get(1)).getChildren().get(game.getActualPlayerID()).setVisible(true);
            nextPlayer.setDisable(false);
        }
        else ((VBox) fieldPanes.get(newIdOfActuallyPane).getChildren().get(0)).getChildren().get(game.getActualPlayerID()).setVisible(true);
    }

    public void hideMoveButton(){
        moveButton.setDisable(true);
    }

    public void canTakeChanceCard(){
        chanceCardButton.setDisable(false);
    }

    public void canTakeSocialCashCard(){
        socialCashCardButton.setDisable(false);
    }

    private void showChanceCard(){
        this.description.setText(game.getChanceCardDescription());
        title.setText("SZANSA");
        showChanceOrSocialCashCard.setDisable(false);
        showChanceOrSocialCashCard.setVisible(true);
    }

    private void showSocialCashCard(){
        this.description.setText(game.getSocialCashCardDescription());
        title.setText("KASA SPOŁECZNA");
        showChanceOrSocialCashCard.setDisable(false);
        showChanceOrSocialCashCard.setVisible(true);
    }

    private void showAlert(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(mainController.getPrimaryStage());
        alert.setTitle("Not enough money");
        alert.setHeaderText("You haven't got enough money to buy it!");

        alert.showAndWait();
    }

    private void handleClickOnField(Object x){
        FieldPane fieldPane = (FieldPane) x;
        String backgroundPath = fieldPane.getPreviewPath();
        preview.setStyle(" -fx-background-image: url('" + backgroundPath + "');  "
                + "-fx-background-position: center center; "+"-fx-background-size: cover;"
                + "-fx-background-repeat: stretch;");

        if(fieldPane.getField() instanceof PossibleToBuyField){
            PossibleToBuyField possibleToBuyField = (PossibleToBuyField) fieldPane.getField();
            actualPossibleToBuyField=possibleToBuyField;
                if(possibleToBuyField.getOwner()==null) buyButton.setDisable(false);
                if(possibleToBuyField.getOwner()==game.getActualPlayer() && game.checkForSell(possibleToBuyField)) sellButton.setDisable(false);

        }
        preview.setVisible(true);
        closePreview.setVisible(true);
        closePreview.setDisable(false);
        Player owner = ((PossibleToBuyField) (((FieldPane) x).getField())).getOwner();
        if(owner ==null) this.owner.setText("");
        else this.owner.setText(owner.getName());
    }

    private void handleClosePreview(){
        preview.setVisible(false);
        sellButton.setDisable(true);
        buyButton.setDisable(true);

    }

    private void handleCloseStage(){
        mainController.getPrimaryStage().close();
    }

    public void setActualPlayer(String playerName){
        actualPlayer.setText(playerName);
    }

    void setFieldPanes(ArrayList<FieldPane> fieldPanes) {
        this.fieldPanes = fieldPanes;
        game.setArrayOfFields(fieldPanes);
    }

    void setRightPlayerPanes(VBox rightPlayerPanes) {
        this.rightPlayerPanes = rightPlayerPanes;
    }

    void setLeftPlayerPanes(VBox leftPlayerPanes) {
        this.leftPlayerPanes = leftPlayerPanes;
    }

    void setNumberOfPlayers(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public void setActualPossibleToBuyField(PossibleToBuyField possibleToBuyField){
        actualPossibleToBuyField=possibleToBuyField;
    }
}
