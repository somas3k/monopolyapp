package pl.lab.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import pl.lab.MainApp;
import pl.lab.model.Game;
import pl.lab.model.Player;

import java.io.IOException;
import java.util.ArrayList;


class PlayerChooser {
    private Stage primaryStage;
    private MainController mainController;
    private VBox leftPlayerPanes;
    private VBox rightPlayerPanes;
    private TextField numberOfPlayers;
    private Button confirmNumberOfPlayers;
    private ImageView logo;
    private Pane chooseNumberOfPlayers;
    private Pane enterNamesOfPlayers;
    private Pane startPane;
    private BorderPane gamePane;
    private GridPane xxx;
    private ArrayList<TextField> names;
    private ArrayList<Player> players;
    private ArrayList<String> parsedNames;
    private GameFX gameFX;
    private Game game;

    private int numOfPlayers;


    PlayerChooser(MainController mainController, GameFX gameFX, Game game){
        this.mainController=mainController;
        this.gameFX=gameFX;
        this.game=game;
        initialize();
    }

    private void initialize(){
        initializeComponents();

    }
    private void initializeComponents(){
        primaryStage=mainController.getPrimaryStage();
        leftPlayerPanes=mainController.getLeftPlayerPanes();
        rightPlayerPanes=mainController.getRightPlayerPanes();
        numberOfPlayers=mainController.getNumberOfPlayers();
        numberOfPlayers.addEventFilter(KeyEvent.KEY_PRESSED,event->handleEnter(event.getCode()));
        confirmNumberOfPlayers=mainController.getConfirmNumberOfPlayers();

        confirmNumberOfPlayers.setOnAction(event->handleConfirmNumberOfPlayers());
        logo=mainController.getLogo();
        chooseNumberOfPlayers=mainController.getChooseNumberOfPlayers();
        enterNamesOfPlayers=mainController.getEnterNamesOfPlayers();
        startPane=mainController.getStartPane();
        gamePane=mainController.getGamePane();
        xxx=mainController.getXxx();


    }

    private void handleEnter(KeyCode code){
        if(code==KeyCode.ENTER) handleConfirmNumberOfPlayers();
    }


    private void handleConfirmNumberOfPlayers(){

        String errorMessage = "";
        if (numberOfPlayers.getText() == null || numberOfPlayers.getText().length() == 0)
            errorMessage = "Wrong number of players";
        else {
            try {
                numOfPlayers = Integer.parseInt(numberOfPlayers.getText());
                if (numOfPlayers < 2 || numOfPlayers > 6) errorMessage = "Wrong number of players";
            } catch (NumberFormatException e) {
                errorMessage = "Wrong number of players";
            }
        }
        if (errorMessage.length() == 0) {
            gameFX.setNumberOfPlayers(numOfPlayers);
            game.setNumberOfPlayers(numOfPlayers);
            gameFX.setPawnsOnStartPane();
            chooseNumberOfPlayers.setDisable(true);
            chooseNumberOfPlayers.setVisible(false);

            setGridPane();
            enterNamesOfPlayers.setVisible(true);
            enterNamesOfPlayers.setDisable(false);

        } else {
            showAlert(errorMessage);
        }


    }

    private void setGridPane(){
        names = new ArrayList<>();
        for(int i=0;i<numOfPlayers;++i){
            Label l = new Label("Player "+Integer.toString(i+1)+":");
            l.setFont(new Font(36));
            xxx.add(l,0,i);
            TextField t = new TextField();
            t.setPromptText("Name");
            t.setPrefColumnCount(10);
            t.setId(Integer.toString(i+1));
            t.setFont(new Font(28));
            names.add(t);
            xxx.add(t,1,i);
        }
        Button confirmButton =new Button("Confirm");
        confirmButton.setOnAction(event->handleConfirmNames());
        confirmButton.setFont(new Font(28));
        xxx.add(confirmButton ,1,numOfPlayers);
    }


    private void handleConfirmNames(){
        parsedNames=new ArrayList<>();
        parsedNames=parseNames();
        if(parsedNames.size()!=numOfPlayers) parsedNames.clear();
        else {
            ArrayList<String> colours = new ArrayList<>();
            colours.add("violet");
            colours.add("black");
            colours.add("blue");
            colours.add("red");
            colours.add("green");
            colours.add("yellow");
            players = new ArrayList<>();
            int i = 0;
            for (String t : parsedNames)
                players.add(new Player(200, t,colours.get(i++)));
            game.setPlayers(players);
            initializePlayerPanes();
            startPane.setDisable(true);
            startPane.setVisible(false);

            gamePane.setVisible(true);
            gamePane.setOpacity(0.5);
            mainController.getStartButton().setVisible(true);
            mainController.getStartButton().setDisable(false);


        }

    }

    private void initializePlayerPanes(){

        for(int i=0;i<numOfPlayers;++i){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/PlayerPane.fxml"));
            Pane t=null;
            try {
                t =  loader.load();
            }
            catch (IOException e){
                e.printStackTrace();
            }

            PlayerPaneController playerPaneController = loader.getController();
            playerPaneController.setOwner(players.get(i));
            playerPaneController.init();

            if(i%2==0) leftPlayerPanes.getChildren().add(t);
            else rightPlayerPanes.getChildren().add(t);

            gameFX.setLeftPlayerPanes(leftPlayerPanes);
            gameFX.setRightPlayerPanes(rightPlayerPanes);
        }

    }

    private ArrayList<String> parseNames(){

        for(TextField t :names){
            if(t.getText()==null || t.getText().length()==0) showAlert("You can't leave the field(s) blank");
            else{
                if(parsedNames.contains(t.getText())) showAlert("You can't create players with the same names");
                else parsedNames.add(t.getText());
            }
        }
        return parsedNames;
    }

    private void showAlert(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Invalid Field");
        alert.setHeaderText("Please correct invalid field(s)");
        alert.setContentText(message);

        alert.showAndWait();
    }


}
