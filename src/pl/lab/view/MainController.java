package pl.lab.view;


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class MainController {
    @FXML
    private VBox leftPlayerPanes;
    @FXML
    private VBox rightPlayerPanes;
    @FXML
    private Button closeStage;
    @FXML
    private Button closeStage11;
    @FXML
    private TextField numberOfPlayers;
    @FXML
    private Button confirmNumberOfPlayers;
    @FXML
    private ImageView logo;
    @FXML
    private Pane chooseNumberOfPlayers;
    @FXML
    private Pane enterNamesOfPlayers;
    @FXML
    private Pane startPane;
    @FXML
    private BorderPane gamePane;
    @FXML
    private GridPane xxx;
    @FXML
    private Button nextPlayer;
    @FXML
    private Button startButton;
    @FXML
    private Button diceButton;
    @FXML
    private Label actualPlayer;
    @FXML
    private Button moveButton;

    private Stage primaryStage;

    Stage getPrimaryStage() {
        return primaryStage;
    }

    BorderPane getGamePane() {
        return gamePane;
    }

    Button getCloseStage() {
        return closeStage;
    }

    Button getCloseStage11() {
        return closeStage11;
    }

    public ImageView getLogo() {
        return logo;
    }

    VBox getLeftPlayerPanes() {
        return leftPlayerPanes;
    }

    VBox getRightPlayerPanes() {
        return rightPlayerPanes;
    }

    TextField getNumberOfPlayers() {
        return numberOfPlayers;
    }

    Button getConfirmNumberOfPlayers() {
        return confirmNumberOfPlayers;
    }

    Pane getChooseNumberOfPlayers() {
        return chooseNumberOfPlayers;
    }

    Pane getEnterNamesOfPlayers() {
        return enterNamesOfPlayers;
    }

    Pane getStartPane() {
        return startPane;
    }

    GridPane getXxx() {
        return xxx;
    }

    Button getNextPlayer() {
        return nextPlayer;
    }

    Button getStartButton(){ return startButton; }

    Button getDiceButton() {
        return diceButton;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    Label getActualPlayer(){
        return actualPlayer;
    }

    Button getMoveButton() {
        return moveButton;
    }
}
