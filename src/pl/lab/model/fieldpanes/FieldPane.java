package pl.lab.model.fieldpanes;

import javafx.scene.layout.Pane;
import pl.lab.model.fields.Field;


public class FieldPane extends Pane {

    private String previewPath;
    private Field field;

    FieldPane(String backgroundPath, double height, double width, String id, Field field){

        this.setPrefHeight(height);
        this.setPrefWidth(width);
        this.setId(id);
        this.setStyle(" -fx-background-image: url('" + backgroundPath + "');  "
                + "-fx-background-position: center center; "+"-fx-background-size: cover;"
                + "-fx-background-repeat: stretch;");
        this.previewPath = null;
        this.field=field;
        this.setDisable(true);

    }
    FieldPane(String backgroundPath, String previewPath, double height, double width, String id, Field field){

        this.setPrefHeight(height);
        this.setPrefWidth(width);
        this.setId(id);
        this.setStyle(" -fx-background-image: url('" + backgroundPath + "');  "
                + "-fx-background-position: center center; "+"-fx-background-size: cover;"
                + "-fx-background-repeat: stretch;");
        this.previewPath = previewPath;
        this.field=field;

    }

    public Field getField() {
        return field;
    }

    public String getPreviewPath() {
        return previewPath;
    }
}
