package pl.lab.model.fieldpanes;

import pl.lab.MainApp;
import pl.lab.model.Pawns;
import pl.lab.model.fields.*;

import java.util.ArrayList;


public class FieldPaneFactory {
    public static ArrayList<FieldPane> createArrayFieldPane(){
        ArrayList<FieldPane> arrayOfFieldPanes = new ArrayList<>();

        FieldPane field = new FieldPane(MainApp.class.getResource("/left/Ulica_Konopacka.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Konopacka.png").toExternalForm(),75.0,120.0,"Ulica Konopacka",new FieldWithImmovables(60,2,30,50,10,30,90,160,250,1));
        Pawns pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        arrayOfFieldPanes.add(field);
        field = new FieldPane(MainApp.class.getResource("/left/Kasa_Spoleczna.png").toExternalForm(),75.0,120.0,"Kasa Społeczna",new SocialCashField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Ulica_Stalowa.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Stalowa.png").toExternalForm(),75.0,120.0,"Ulica Stalowa",new FieldWithImmovables(60,4,30,50,20,60,180,320,450,1));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Podatek_Dochodowy.png").toExternalForm(),75.0,120.0,"Podatek Dochodowy",new ChargingField(200));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Dworzec_Zachodni.png").toExternalForm(),MainApp.class.getResource("/previews/Dworzec_Zachodni.png").toExternalForm(),75.0,120.0,"Dworzec Zachodni",new RailwayStationField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Ulica_Radzyminska.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Radzyminska.png").toExternalForm(),75.0,120.0,"Ulica Radzymińska",new FieldWithImmovables(100,6,50,50,30,90,270,400,550,2));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Szansa.png").toExternalForm(),75.0,120.0,"Szansa",new ChanceField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Ulica_Jagiellonska.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Jagiellonska.png").toExternalForm(),75.0,120.0,"Ulica Jagiellońska",new FieldWithImmovables(100,6,50,50,30,90,270,400,550,2));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/left/Ulica_Targowa.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Targowa.png").toExternalForm(),75.0,120.0,"Ulica Targowa",new FieldWithImmovables(120,8,60,50,40,100,300,450,600,2));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(99.5,4.5,0);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Wiezienie2.png").toExternalForm(),120.0,120.0,"Więzienie",new PrisonField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(55,-17,90);
        field.getChildren().add(0,pawns);
        pawns= new Pawns(55,27,45);
        field.getChildren().add(1,pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Ulica_Plowiecka.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Plowiecka.png").toExternalForm(),120.0,75.0,"Ulica Płowiecka", new FieldWithImmovables(140,10,70,100,50,150,450,625,750,3));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Elektrownia.png").toExternalForm(),MainApp.class.getResource("/previews/Elektrownia.png").toExternalForm(),120.0,75.0,"Elektrownia",new PublicUtilityField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Ulica_Marsa.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Marsa.png").toExternalForm(),120.0,75.0,"Ulica Marsa",new FieldWithImmovables(140,10,70,100,50,150,450,625,750,3));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Ulica_Grochowska.png").toExternalForm(), MainApp.class.getResource("/previews/Ulica_Grochowska.png").toExternalForm(), 120.0, 75.0, "Ulica Grochowska", new FieldWithImmovables(160,12,80,100,60,180,500,700,900,3) );
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Dworzec_Gdański.png").toExternalForm(),MainApp.class.getResource("/previews/Dworzec_Gdański.png").toExternalForm(),120.0,75.0,"Dworzec Gdański",new RailwayStationField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Ulica_Obozowa.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Obozowa.png").toExternalForm(),120.0,75.0,"Ulica Obozowa",new FieldWithImmovables(180,14,90,100,70,200,550,750,950,4));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Kasa_Spoleczna.png").toExternalForm(),120.0,75.0,"Kasa Społeczna",new SocialCashField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Ulica_Gorczewska.png").toExternalForm(), MainApp.class.getResource("/previews/Ulica_Gorczewska.png").toExternalForm(), 120.0, 75.0, "Ulica Górczewska", new FieldWithImmovables(180,14,90,100,70,200,550,750,950,4));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Ulica_Wolska.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Wolska.png").toExternalForm(),120.0,75.0,"Ulica Wolska",new FieldWithImmovables(200,16,100,100,80,220,600,800,1000,4));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,72,90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/top/Parking.png").toExternalForm(),120.0,120,"Parking",new ParkingField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(50,30,135);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Ulica_Mickiewicza.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Mickiewicza.png").toExternalForm(),75.0,120,"Ulica Mickiewicza",new FieldWithImmovables(220,18,110,150,90,250,700,875,1050,5));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Szansa2.png").toExternalForm(),75.0,120,"Szansa",new ChanceField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Ulica_Słowackiego.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Słowackiego.png").toExternalForm(),75.0,120,"Ulica Słowackiego",new FieldWithImmovables(220,18,110,150,90,250,700,875,1050,5));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Plac_Wilsona.png").toExternalForm(),MainApp.class.getResource("/previews/Plac_Wilsona.png").toExternalForm(),75.0,120,"Plac Wilsona",new FieldWithImmovables(240,20,120,150,100,300,750,925,1100,5));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Dworzec_Wschodni.png").toExternalForm(),MainApp.class.getResource("/previews/Dworzec_Wschodni.png").toExternalForm(),75.0,120,"Dworzec Wschodni",new RailwayStationField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Ulica_Swietokrzyska.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Swietokrzyska.png").toExternalForm(),75.0,120,"Ulica Świętokrzyska",new FieldWithImmovables(260,22,130,150,110,330,800,975,1150,6));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Krakowskie_Przedmiescie.png").toExternalForm(),MainApp.class.getResource("/previews/Krakowskie_Przedmiescie.png").toExternalForm(),75.0,120,"Krakowskie Przedmieście",new FieldWithImmovables(260,22,130,150,110,330,800,975,1150,6));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/right/Wodociagi.png").toExternalForm(),MainApp.class.getResource("/previews/Wodociagi.png").toExternalForm(),75.0,120,"Wodociagi",new PublicUtilityField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field  = new FieldPane(MainApp.class.getResource("/right/Nowy_Swiat.png").toExternalForm(),MainApp.class.getResource("/previews/Nowy_Swiat.png").toExternalForm(),75.0,120,"Nowy Świat",new FieldWithImmovables(280,24,140,150,120,360,850,1025,1200,6));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(9.5,4.5,180);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Wiezienie.png").toExternalForm(),120.0,120,"Idź do więzienia",new GoToPrisonField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(50,30,-135);
        field.getChildren().add(pawns); //X=50 Y=30 rotate = -135
        field = new FieldPane(MainApp.class.getResource("/bottom/Plac_Trzech_Krzyzy.png").toExternalForm(),MainApp.class.getResource("/previews/Plac_Trzech_Krzyzy.png").toExternalForm(),120.0,75,"Plac Trzech Krzyży",new FieldWithImmovables(300,26,150,200,130,390,900,1100,1275,7));
        arrayOfFieldPanes.add(field);//X=9.5 Y=4.5
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Ulica_Marszalkowska.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Marszalkowska.png").toExternalForm(),120.0,75,"Ulica Marszalkowska",new FieldWithImmovables(300,26,150,200,130,390,900,1100,1275,7));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Kasa_Spoleczna.png").toExternalForm(),120.0,75,"Kasa Społeczna",new SocialCashField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Aleje_Jerozolimskie.png").toExternalForm(),MainApp.class.getResource("/previews/Aleje_Jerozolimskie.png").toExternalForm(),120.0,75,"Aleje Jerozolimskie",new FieldWithImmovables(320,28,160,200,150,450,1000,1200,1400,7));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Dworzec_Centralny.png").toExternalForm(),MainApp.class.getResource("/previews/Dworzec_Centralny.png").toExternalForm(),120.0,75,"Dworzec Centralny",new RailwayStationField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Szansa3.png").toExternalForm(),120.0,75,"Szansa",new ChanceField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Ulica_Belwederska.png").toExternalForm(),MainApp.class.getResource("/previews/Ulica_Belwederska.png").toExternalForm(),120.0,75,"Ulica Belwederska",new FieldWithImmovables(350,35,175,200,175,500,1100,1300,1500,8));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Domiar_Podatkowy.png").toExternalForm(),120.0,75,"Domiar Podatkowy",new ChargingField(100));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Aleje_Ujazdowskie.png").toExternalForm(),MainApp.class.getResource("/previews/Aleje_Ujazdowskie.png").toExternalForm(),120.0,75,"Aleje Ujazdowskie",new FieldWithImmovables(400,50,200,200,200,600,1400,1700,2000,8));
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(32,-18,-90);
        field.getChildren().add(pawns);
        field = new FieldPane(MainApp.class.getResource("/bottom/Start.png").toExternalForm(),120.0,120,"Start",new StartField());
        arrayOfFieldPanes.add(field);
        pawns= new Pawns(30,50,-45);
        field.getChildren().add(pawns);

        return arrayOfFieldPanes;
    }
}
