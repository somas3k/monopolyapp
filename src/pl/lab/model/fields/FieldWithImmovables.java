package pl.lab.model.fields;

/**
 * Created by Somas3k on 13.01.2017.
 */
public class FieldWithImmovables extends PossibleToBuyField {
    private int houses;
    private int hotels;
    private int housePrice;
    private int hotelPrice;
    private int rentWithOneHouse;
    private int rentWithTwoHouses;
    private int rentWithThreeHouses;
    private int rentWithFourHouses;
    private int rentWithHotel;
    private int streetID;

    public FieldWithImmovables(int price,int rent,int mortageValue,int housePrice, int rentWithOneHouse, int rentWithTwoHouses, int rentWithThreeHouses, int rentWithFourHouses, int rentWithHotel, int streetID) {

        this.housePrice = housePrice;
        this.rentWithOneHouse = rentWithOneHouse;
        this.rentWithTwoHouses = rentWithTwoHouses;
        this.rentWithThreeHouses = rentWithThreeHouses;
        this.rentWithFourHouses = rentWithFourHouses;
        this.rentWithHotel = rentWithHotel;
        this.streetID = streetID;
        this.hotelPrice=this.housePrice*5;
        this.houses=0;
        this.hotels=0;
        this.setPrice(price);
        this.setMortgageValue(mortageValue);
        this.setRent(rent);
        this.setOwner(null);
        this.setPledged(false);
    }

    public int getHouses() {
        return houses;
    }

    public int getHotels() {
        return hotels;
    }

    public int getHousePrice() {
        return housePrice;
    }

    public int getHotelPrice() {
        return hotelPrice;
    }

    public int getRentWithOneHouse() {
        return rentWithOneHouse;
    }

    public int getRentWithTwoHouses() {
        return rentWithTwoHouses;
    }

    public int getRentWithThreeHouses() {
        return rentWithThreeHouses;
    }

    public int getRentWithFourHouses() {
        return rentWithFourHouses;
    }

    public int getRentWithHotel() {
        return rentWithHotel;
    }

    public int getStreetID() {
        return streetID;
    }
}
