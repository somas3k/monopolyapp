package pl.lab.model.fields;


public class ChargingField extends Field {
    private int charge;

    public ChargingField(int charge) {
        this.charge = charge;
    }

    public int getCharge() {
        return charge;
    }
}
