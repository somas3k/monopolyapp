package pl.lab.model.fields;

/**
 * Created by Somas3k on 13.01.2017.
 */
public class RailwayStationField extends PossibleToBuyField {
    public RailwayStationField() {
        this.setPledged(false);
        this.setPrice(200);
        this.setMortgageValue(100);
        this.setRent(25);
        this.setOwner(null);
    }
}
