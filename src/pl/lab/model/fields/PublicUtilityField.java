package pl.lab.model.fields;

/**
 * Created by Somas3k on 13.01.2017.
 */
public class PublicUtilityField extends PossibleToBuyField {
    public PublicUtilityField() {
        this.setMortgageValue(75);
        this.setPrice(150);
        this.setPledged(false);
        this.setOwner(null);

    }
}
