package pl.lab.model.fields;

import pl.lab.model.Player;

/**
 * Created by Somas3k on 13.01.2017.
 */
public abstract class PossibleToBuyField extends Field {
    private Player owner;
    private int price;
    private int rent;
    private int mortgageValue;
    private boolean pledged;

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public int getMortgageValue() {
        return mortgageValue;
    }

    public void setMortgageValue(int mortgageValue) {
        this.mortgageValue = mortgageValue;
    }

    public boolean isPledged() {
        return pledged;
    }

    public void setPledged(boolean pledged) {
        this.pledged = pledged;
    }
}
