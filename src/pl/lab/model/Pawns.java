package pl.lab.model;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;


public class Pawns extends VBox {

    public Pawns(double layoutX,double layoutY,double rotate){
        Circle pawn = new Circle(5, Paint.valueOf("violet"));
        pawn.setStroke(Paint.valueOf("white"));
        pawn.setVisible(false);
        this.getChildren().add(pawn);
        pawn = new Circle(5, Paint.valueOf("black"));
        pawn.setStroke(Paint.valueOf("white"));
        pawn.setVisible(false);
        this.getChildren().add(pawn);
        pawn = new Circle(5, Paint.valueOf("blue"));
        pawn.setStroke(Paint.valueOf("white"));
        pawn.setVisible(false);
        this.getChildren().add(pawn);
        pawn = new Circle(5, Paint.valueOf("red"));
        pawn.setStroke(Paint.valueOf("white"));
        pawn.setVisible(false);
        this.getChildren().add(pawn);
        pawn = new Circle(5, Paint.valueOf("green"));
        pawn.setStroke(Paint.valueOf("white"));
        pawn.setVisible(false);
        this.getChildren().add(pawn);
        pawn = new Circle(5, Paint.valueOf("yellow"));
        pawn.setStroke(Paint.valueOf("white"));
        pawn.setVisible(false);
        this.getChildren().add(pawn);

        this.setLayoutX(layoutX);
        this.setLayoutY(layoutY);
        this.setRotate(rotate);
    }
}
