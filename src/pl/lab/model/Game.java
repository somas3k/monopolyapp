package pl.lab.model;

import pl.lab.MainApp;
import pl.lab.model.cards.Cards;
import pl.lab.model.cards.chance.*;
import pl.lab.model.cards.socialcash.*;
import pl.lab.model.fieldpanes.FieldPane;
import pl.lab.model.fields.*;
import pl.lab.view.GameFX;
import pl.lab.view.MainController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;


public class Game {
    private ArrayList<FieldPane> arrayOfFields;
    private ArrayList<Player> players;
    private LinkedList<SocialCashCard> socialCashCards;
    private LinkedList<ChanceCard> chanceCards;
    private MainController mainController;
    private int numberOfPlayers;
    private GameFX gameFX;
    private int actualPlayer;
    private Random random;
    private ArrayList<Dice> dices;
    private int countOfDoublets;
    private int[] countOfMoves=new int[2];


    public Game(MainController mainController){
        this.mainController=mainController;
        actualPlayer=0;
    }

    public void run(){
        gameFX = new GameFX(this,mainController);
        random = new Random();
        socialCashCards= Cards.generateSocialCashCards();
        chanceCards=Cards.generateChanceCards();
        setDices();
        countOfDoublets=0;
    }

    public int[] rollTheDice(){
        countOfMoves[0]=random.nextInt(6);
        countOfMoves[1]=random.nextInt(6);
        /*countOfMoves[0]=3;
        countOfMoves[1]=2;*/
        return countOfMoves;
    }

    public void checkRollTheDice(){
        if(countOfMoves[0]==countOfMoves[1]){
            countOfDoublets++;
            if(countOfDoublets==3){
                goToPrison();
                countOfDoublets=0;
            }
            else gameFX.canMove();
        }
        else{
            countOfDoublets=0;
            gameFX.canMove();
        }
    }

    public void nextPlayer(){
        actualPlayer = actualPlayer == numberOfPlayers-1 ? 0 : actualPlayer+1;
        gameFX.setActualPlayer(getPlayerName(actualPlayer));
        gameFX.canRollTheDice();
        gameFX.hideNextPlayerButton();
    }

    public void move(boolean fromCard){
        gameFX.hideMoveButton();
        if(!fromCard) {
            int idOfActuallyField;
            if(players.get(actualPlayer).isPrisoner()) idOfActuallyField=9;
            else idOfActuallyField = players.get(actualPlayer).getIdOfActuallyField();
            int newIdOfActuallyField = idOfActuallyField + countOfMoves[0] + 1 + countOfMoves[1] + 1 ;
            updateIdOfActuallyField(players.get(actualPlayer), newIdOfActuallyField, false);
        }

        Field actualField=arrayOfFields.get(players.get(actualPlayer).getIdOfActuallyField()).getField();
        if( actualField instanceof ChanceField){
            gameFX.canTakeChanceCard();
        }
        if(actualField instanceof SocialCashField)
            gameFX.canTakeSocialCashCard();

        if(actualField instanceof ChargingField){
            players.get(actualPlayer).decreaseBalance(((ChargingField) actualField).getCharge());
            gameFX.showNextPlayerButton();
        }

        if(actualField instanceof ParkingField) gameFX.showNextPlayerButton();

        if(actualField instanceof StartField) gameFX.showNextPlayerButton();

        if(actualField instanceof PrisonField) gameFX.showNextPlayerButton();

        if(actualField instanceof GoToPrisonField){
            goToPrison();
            nextPlayer();
        }

        if(actualField instanceof PossibleToBuyField){
            execActionsWithPossibleToBuyField((PossibleToBuyField) actualField);
            gameFX.showNextPlayerButton();
        }

        if(countOfDoublets!=0){
            gameFX.hideNextPlayerButton();
            gameFX.canRollTheDice();
        }
    }

    private void execActionsWithPossibleToBuyField(PossibleToBuyField possibleToBuyField){
        gameFX.setActualPossibleToBuyField(possibleToBuyField);
        if(possibleToBuyField.getOwner()!=null && !possibleToBuyField.isPledged()){
            int rent = calculateRent(possibleToBuyField);
            players.get(actualPlayer).decreaseBalance(rent);
            possibleToBuyField.getOwner().increaseBalance(rent);
        }

    }

    public boolean buyField(PossibleToBuyField possibleToBuyField){
        Player player=players.get(actualPlayer);
        if(player.getBalance()<possibleToBuyField.getPrice()) return false;
        else {
            possibleToBuyField.setOwner(players.get(actualPlayer));
            players.get(actualPlayer).decreaseBalance(possibleToBuyField.getPrice());
            players.get(actualPlayer).getOwnFields().add(possibleToBuyField);
            return true;
        }
    }

    public void sellField(PossibleToBuyField possibleToBuyField){
        possibleToBuyField.setPledged(true);
        players.get(actualPlayer).increaseBalance(possibleToBuyField.getMortgageValue());

    }

    public boolean checkForSell(PossibleToBuyField possibleToBuyField){
        boolean check = true;
        if(possibleToBuyField.isPledged()) check=false;
        else {
            if (possibleToBuyField instanceof FieldWithImmovables) {
                FieldWithImmovables fieldWithImmovables = (FieldWithImmovables) possibleToBuyField;
                if (fieldWithImmovables.getHouses() != 0 || fieldWithImmovables.getHotels() != 0) check = false;
            }
        }
        return check;
    }

    private int calculateRent(PossibleToBuyField possibleToBuyField){
        if(possibleToBuyField instanceof FieldWithImmovables){
            FieldWithImmovables fieldWithImmovables = (FieldWithImmovables) possibleToBuyField;
            if(fieldWithImmovables.getHouses()==0 && fieldWithImmovables.getHotels()==0) return possibleToBuyField.getRent();
            if(fieldWithImmovables.getHotels()==1) return fieldWithImmovables.getRentWithHotel();
            switch(fieldWithImmovables.getHouses()) {
                case 1: return fieldWithImmovables.getRentWithOneHouse();
                case 2: return fieldWithImmovables.getRentWithTwoHouses();
                case 3: return fieldWithImmovables.getRentWithThreeHouses();
                case 4: return fieldWithImmovables.getRentWithFourHouses();
            }
        }
        return possibleToBuyField.getRent();

    }

    private void goToPrison(){
        updateIdOfActuallyField(players.get(actualPlayer),40,true);
        players.get(actualPlayer).setPrisoner(true);
    }

    public void execChanceCard(){
        ChanceCard chanceCard=chanceCards.poll();
        chanceCards.addLast(chanceCard);
        if(chanceCard instanceof ChargingChanceCard) {
            players.get(actualPlayer).decreaseBalance(((ChargingChanceCard) chanceCard).getCharge());
            gameFX.showNextPlayerButton();
        }
        if(chanceCard instanceof TakingMoneyChanceCard) {
            players.get(actualPlayer).increaseBalance(((TakingMoneyChanceCard) chanceCard).getPrize());
            gameFX.showNextPlayerButton();
        }
        if(chanceCard instanceof ActionChanceCard){
            ActionChanceCard actionChanceCard=(ActionChanceCard) chanceCard;
            if(actionChanceCard.getGoBack()==0){
                updateIdOfActuallyField(players.get(actualPlayer),actionChanceCard.getGoTo(),false);
                gameFX.showNextPlayerButton();
                move(true);
            }
            if(actionChanceCard.getGoBack()==-1) updateIdOfActuallyField(players.get(actualPlayer),actionChanceCard.getGoTo(),true);
            if(actionChanceCard.getGoBack()>0){
                updateIdOfActuallyField(players.get(actualPlayer),players.get(actualPlayer).getIdOfActuallyField()-actionChanceCard.getGoBack(),false);
                gameFX.showNextPlayerButton();
                move(true);
            }
        }

        if(chanceCard instanceof SpecialChanceCard){
            players.get(actualPlayer).setSpecialChanceCard((SpecialChanceCard) chanceCard);
            chanceCards.removeLast();
            gameFX.showNextPlayerButton();
        }
    }

    public void execSocialCashCard(){
        SocialCashCard socialCashCard=socialCashCards.poll();
        socialCashCards.addLast(socialCashCard);
        if(socialCashCard instanceof ChargingSocialCashCard) {
            players.get(actualPlayer).decreaseBalance(((ChargingSocialCashCard) socialCashCard).getCharge());
            gameFX.showNextPlayerButton();
        }

        if(socialCashCard instanceof TakingMoneySocialCashCard) {
            players.get(actualPlayer).increaseBalance(((TakingMoneySocialCashCard) socialCashCard).getPrize());
            gameFX.showNextPlayerButton();
        }

        if(socialCashCard instanceof ActionSocialCashCard){
            ActionSocialCashCard actionSocialCashCard=(ActionSocialCashCard) socialCashCard;
            if(actionSocialCashCard.getGoBack()==0){
                updateIdOfActuallyField(players.get(actualPlayer),actionSocialCashCard.getGoTo(),false);
                gameFX.showNextPlayerButton();
                move(true);
            }
            if(actionSocialCashCard.getGoBack()==-1) updateIdOfActuallyField(players.get(actualPlayer),actionSocialCashCard.getGoTo(),true);
            if(actionSocialCashCard.getGoBack()>0){
                updateIdOfActuallyField(players.get(actualPlayer),players.get(actualPlayer).getIdOfActuallyField()-actionSocialCashCard.getGoBack(),false);
                gameFX.showNextPlayerButton();
                move(true);

            }
        }

        if(socialCashCard instanceof SpecialSocialCashCard) {
            players.get(actualPlayer).setSpecialSocialCashCard((SpecialSocialCashCard) socialCashCard);
            socialCashCards.removeLast();
            gameFX.showNextPlayerButton();
        }
    }

    private void updateIdOfActuallyField(Player player,int newIdOfActuallyField,boolean isPrisoner) {
        int idOfActuallyField;
        if(player.isPrisoner()){
            idOfActuallyField = 9;


        }
        else idOfActuallyField = player.getIdOfActuallyField();
        if (isPrisoner) {
            player.setIdOfActuallyField(newIdOfActuallyField);
            gameFX.setPawnOnPane(idOfActuallyField, 40);
            player.setPrisoner(true);
        } else {
            if (newIdOfActuallyField > 39) {
                newIdOfActuallyField = newIdOfActuallyField - 40;
                players.get(actualPlayer).increaseBalance(200);
            }
            player.setIdOfActuallyField(newIdOfActuallyField);
            if(player.isPrisoner()) {
                gameFX.setPawnOnPane(40, newIdOfActuallyField);
                player.setPrisoner(false);
            }
            else
                gameFX.setPawnOnPane(idOfActuallyField, newIdOfActuallyField);


        }
    }

    public String getChanceCardDescription(){
        return chanceCards.peek().getDescription();
    }

    public String getSocialCashCardDescription(){
        return socialCashCards.peek().getDescription();
    }

    public void setPlayers(ArrayList<Player> players){
        this.players=players;
    }

    public int getActualPlayerID() {
        return actualPlayer;
    }

    public Player getActualPlayer() { return players.get(actualPlayer);}

    public void setArrayOfFields(ArrayList<FieldPane> arrayOfFields) {
        this.arrayOfFields = arrayOfFields;
    }

    public String getPlayerName(int i){
        return players.get(i).getName();
    }

    public void setNumberOfPlayers(int numberOfPlayers){
        this.numberOfPlayers=numberOfPlayers;
    }

    private void setDices(){
        dices = new ArrayList<>();
        for(int i=0;i<6;++i){
            dices.add(i,new Dice(i+1,MainApp.class.getResource("/dices/"+(i+1)+".png").toExternalForm()));
        }

    }

    public ArrayList<Dice> getDices() {
        return dices;
    }

}
