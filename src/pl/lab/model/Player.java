package pl.lab.model;

import javafx.scene.control.Label;
import pl.lab.model.cards.chance.SpecialChanceCard;
import pl.lab.model.cards.socialcash.SpecialSocialCashCard;
import pl.lab.model.fields.PossibleToBuyField;

import java.util.ArrayList;


public class Player {
    private ArrayList<PossibleToBuyField> ownFields;
    private SpecialChanceCard specialChanceCard;
    private SpecialSocialCashCard specialSocialCashCard;
    private int balance;
    private int idOfActuallyField;
    private String name;
    private String colour;
    private boolean isPrisoner;
    private Label balanceLabel;



    public Player(int balance, String name,String colour) {
        this.balance = 0;
        this.name = name;
        this.colour=colour;
        this.idOfActuallyField=39;
        isPrisoner=false;
        ownFields=new ArrayList<>();
    }

    boolean isPrisoner() {
        return isPrisoner;
    }

    void setPrisoner(boolean prisoner) {
        isPrisoner = prisoner;
    }

    public String getColour() {
        return colour;
    }

    public void setBalanceLabel(Label balanceLabel){
        this.balanceLabel=balanceLabel;
    }

    ArrayList<PossibleToBuyField> getOwnFields() {
        return ownFields;
    }

    public void setOwnFields(ArrayList<PossibleToBuyField> ownFields) {
        this.ownFields = ownFields;
    }

    public SpecialChanceCard getSpecialChanceCard() {
        return specialChanceCard;
    }

    void setSpecialChanceCard(SpecialChanceCard specialChanceCard) {
        this.specialChanceCard = specialChanceCard;
    }

    public SpecialSocialCashCard getSpecialSocialCashCard() {
        return specialSocialCashCard;
    }

    void setSpecialSocialCashCard(SpecialSocialCashCard specialSocialCashCard) {
        this.specialSocialCashCard = specialSocialCashCard;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    int getIdOfActuallyField() {
        return idOfActuallyField;
    }

    void setIdOfActuallyField(int idOfActuallyField) {
        this.idOfActuallyField = idOfActuallyField;
    }

    public String getName() {
        return name;
    }

    void decreaseBalance(int amount){
        balance-=amount;
        balanceLabel.setText(balance+"");
    }
    void increaseBalance(int amount){
        balance+=amount;
        balanceLabel.setText(balance+"");
    }
}
