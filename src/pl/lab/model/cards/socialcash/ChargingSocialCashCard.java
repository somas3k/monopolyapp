package pl.lab.model.cards.socialcash;


public class ChargingSocialCashCard extends SocialCashCard {
    private int charge;
    public ChargingSocialCashCard(String description,int charge){
        super.setDescription(description);
        this.charge=charge;
    }

    public int getCharge() {
        return charge;
    }
}
