package pl.lab.model.cards.socialcash;


public class TakingMoneySocialCashCard extends SocialCashCard {
    private int prize;

    public TakingMoneySocialCashCard(String description,int prize) {
        this.prize = prize;
        super.setDescription(description);
    }

    public int getPrize() {
        return prize;
    }




}
