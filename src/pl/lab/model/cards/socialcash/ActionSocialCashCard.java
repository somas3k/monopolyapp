package pl.lab.model.cards.socialcash;


public class ActionSocialCashCard extends SocialCashCard {
    private int goBack;
    private int goTo;

    public ActionSocialCashCard(String description,int goBack, int goTo) {
        super.setDescription(description);
        this.goBack = goBack;
        this.goTo = goTo;
    }

    public int getGoBack() {
        return goBack;
    }

    public int getGoTo() {
        return goTo;
    }
}
