package pl.lab.model.cards.socialcash;


public abstract class SocialCashCard {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
