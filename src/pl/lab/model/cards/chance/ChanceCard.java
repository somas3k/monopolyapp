package pl.lab.model.cards.chance;


public abstract class ChanceCard {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
