package pl.lab.model.cards.chance;


public class TakingMoneyChanceCard extends ChanceCard {
    private int prize;

    public TakingMoneyChanceCard(String description,int prize) {
        this.setDescription(description);
        this.prize = prize;
    }

    public int getPrize() {
        return prize;
    }
}
