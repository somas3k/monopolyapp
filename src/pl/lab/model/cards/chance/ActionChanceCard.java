package pl.lab.model.cards.chance;


public class ActionChanceCard extends ChanceCard {
    private int goBack;
    private int goTo;

    public ActionChanceCard(String description,int goBack,int goTo){
        this.setDescription(description);
        this.goBack=goBack;
        this.goTo=goTo;
    }

    public int getGoBack() {
        return goBack;
    }

    public int getGoTo() {
        return goTo;
    }
}
