package pl.lab.model.cards.chance;


public class ChargingChanceCard extends ChanceCard {
    private int charge;

    public ChargingChanceCard(String description,int charge) {
        this.charge = charge;
        this.setDescription(description);
    }

    public int getCharge() {
        return charge;
    }
}
