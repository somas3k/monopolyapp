package pl.lab.model.cards;

import pl.lab.model.cards.chance.*;
import pl.lab.model.cards.socialcash.*;

import java.util.Collections;
import java.util.LinkedList;


public class Cards {

    public static LinkedList<SocialCashCard> generateSocialCashCards(){
        LinkedList<SocialCashCard> socialCashCards = new LinkedList<>();
        socialCashCards.add(new ActionSocialCashCard("Przejdź na START.",0,39));
        socialCashCards.add(new ActionSocialCashCard("Wróć na ulicę Konopacką.",0,0));
        socialCashCards.add(new ActionSocialCashCard("Idź do WIĘZIENIA. Przejdź prosto do WIĘZIENIA. Nie przechodź przez START. Nie pobieraj 200 zł.",-1,9));
        socialCashCards.add(new ChargingSocialCashCard("Honorarium lekarza. Zapłać 50 zł.",50));
        socialCashCards.add(new ChargingSocialCashCard("Zapłać rachunek za szpital 100 zł.",100));
        socialCashCards.add(new ChargingSocialCashCard("Zapłać składkę ubezpieczeniową 50 zł.",50));
        socialCashCards.add(new TakingMoneySocialCashCard("Wygrana druga nagroda w konkursie piękności. Pobierz 10 zł.",10));
        socialCashCards.add(new TakingMoneySocialCashCard("Sprzedałeś obligacje. Pobierz 100 zł.",100));
        socialCashCards.add(new TakingMoneySocialCashCard("Błąd bankowy na twoją korzyść. Pobierz 200 zł.",200));
        socialCashCards.add(new SpecialSocialCashCard("WYJDŹ BEZPŁATNIE Z WIĘŹIENIA. Tę kartę możesz zatrzymać do późniejszego wykorzystania."));

        Collections.shuffle(socialCashCards);
        return socialCashCards;
    }

    public static LinkedList<ChanceCard> generateChanceCards(){
        LinkedList<ChanceCard> chanceCards = new LinkedList<>();
        chanceCards.add(new ActionChanceCard("Cofnij się o trzy pola.",3,0));
        chanceCards.add(new ActionChanceCard("Przejdź na START.",0,39));
        chanceCards.add(new ActionChanceCard("Idź do WIĘZIENIA. Przejdź prosto do WIĘZIENIA. Nie przechodź przez START. Nie pobieraj 200 zł.",-1,9));
        chanceCards.add(new ChargingChanceCard("Zapłać za szkodę, 150 zł",150));
        chanceCards.add(new ChargingChanceCard("Mandat za przekroczenie prędkości. Zapłać 15 zł.",15));
        chanceCards.add(new ChargingChanceCard("Grzywna. Zapłać 20 zł.",20));
        chanceCards.add(new TakingMoneyChanceCard("Wygrałeś konkurs krzyżówkowy. Pobierz 100 zł",100));
        chanceCards.add(new TakingMoneyChanceCard("Otrzymałeś kredyt budowlany. Pobierz 150 zł.",150));
        chanceCards.add(new TakingMoneyChanceCard("Bank wypłaca Ci dywidendę w wysokości 50 zł",50));
        chanceCards.add(new SpecialChanceCard("WYJDŹ BEZPŁATNIE Z WIĘŹIENIA. Tę kartę możesz zatrzymać do późniejszego wykorzystania."));

        Collections.shuffle(chanceCards);
        return chanceCards;
    }
}
