package pl.lab.model;

import javafx.scene.image.Image;


public class Dice {
    private final int count;
    private Image image;

    public Dice(int count,String url){
        this.count=count;
        image=new Image(url);
    }

    public int getCount() {
        return count;
    }

    public Image getImage() {
        return image;
    }
}
