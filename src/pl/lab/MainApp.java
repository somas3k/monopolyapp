package pl.lab;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.lab.model.Game;
import pl.lab.view.MainController;

import java.io.IOException;



public class MainApp extends Application {
    private Stage primaryStage;
    private AnchorPane rootLayout;

    public static void main(String[] args) {

        launch(MainApp.class);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("MonopolyApp");
        this.primaryStage.setResizable(true);
        this.primaryStage.setMaximized(true);
        this.primaryStage.setFullScreen(true);
        this.primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);

        initRootLayout();

        this.primaryStage.show();
    }


    private void initRootLayout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
        try {
            // Load root layout from fxml file.

            rootLayout = loader.load();


        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        MainController mainController = loader.getController();
        mainController.getLogo().setImage(new Image(MainApp.class.getResource("/Monopoly.png").toExternalForm()));
        mainController.setPrimaryStage(primaryStage);
        new Game(mainController).run();

    }

}
